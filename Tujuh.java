import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Tujuh {
    public static void main(String[] args) throws IOException {
        InputStreamReader r = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(r);

        System.out.print("Masukkan kalimat = ");
        String s = br.readLine();
        System.out.println(pecah(s));
    }

    public static String pecah(String s) {
        String[] kata = s.split(" ");
        s = "";
        for (String a : kata) {
            for (int i = a.length() - 1; i >= 0; i--) {
                s = s + a.charAt(i);
            }
            s = s + " ";
        }

        return s;
    }
}