import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Satu {
    public static void main(String[] args) throws IOException {
        InputStreamReader r = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(r);

        int angka;
        System.out.print("Masukkan angka = ");
        angka = Integer.parseInt(br.readLine());

        if (angka >= 90) {
            System.out.println("A");
        } else if (angka >= 80) {
            System.out.println("B");
        } else if (angka >= 70) {
            System.out.println("C");
        } else if (angka >= 60) {
            System.out.println("D");
        } else {
            System.out.println("E");
        }
    }
}