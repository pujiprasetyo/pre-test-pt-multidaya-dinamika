import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Delapan {
    public static void main(String[] args) throws IOException {
        ArrayList<Integer> arr = new ArrayList<>();
        int hsl = 0;

        System.out.println("Input angka tanpa  batas");
        terima(arr);
        for (int a : arr) {
            hsl = hsl + a;
        }
        System.out.println(hsl);
    }

    public static ArrayList<Integer> terima(ArrayList<Integer> arr) throws IOException {
        InputStreamReader r = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(r);

        System.out.print("Masukkan angka : ");
        String s = br.readLine();
        if (s.equals("=")) {
            return arr;
        } else {
            arr.add(Integer.parseInt(s));
            terima(arr);
        }
        return new ArrayList<>();
    }
}