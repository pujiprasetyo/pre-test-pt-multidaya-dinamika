import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Dua {
    public static void main(String[] args) throws IOException {
        InputStreamReader r = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(r);

        int angka;
        System.out.print("Masukkan angka = ");
        angka = Integer.parseInt(br.readLine());

        if (angka % 2 == 0) {
            System.out.println("Bilangan Genap");
        } else {
            System.out.println("Bilangan Ganjil");
        }
    }
}