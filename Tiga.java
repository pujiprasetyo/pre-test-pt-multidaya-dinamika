import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class Tiga {
    public static void main(String[] args) throws IOException {
        InputStreamReader r = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(r);

        ArrayList<Integer> arr = new ArrayList<>();
        int n;
        double sum = 0;

        System.out.print("Jumlah data = ");
        n = Integer.parseInt(br.readLine());

        for (int i = 0; i < n; i++) {
            System.out.print("Masukkan angka = ");
            arr.add(Integer.parseInt(br.readLine()));
        }

        for (int a : arr) {
            System.out.println(a);
            sum = sum + a;
        }

        System.out.println("Nilai maksimum = " + Collections.max(arr));
        System.out.println("Nilai minimum = " + Collections.min(arr));
        System.out.println("Nilai rata-rata = " + sum / n);
    }
}