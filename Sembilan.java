import java.util.HashSet;
import java.util.Set;

public class Sembilan {
    public static void main(String[] args) {
        String[] fileJson = { "Jakarta", "Aceh", "Malang", "Medan", "Bontang", "Jogja", "Jakarta", "Bandung", "Malang",
                "Solo", "Palembang", "Bandung" };
        Set<String> set = new HashSet<>();

        for (String s : fileJson) {
            set.add(s);
        }

        System.out.println(set);
    }
}