import java.util.ArrayList;
import java.util.HashMap;

public class Sepuluh {
    public static void main(String[] args) {
        ArrayList<HashMap<String, String>> arr = fileJson();
        ArrayList<HashMap<String, String>> hsl = new ArrayList<>();
        for (HashMap<String, String> map : arr) {
            if (Integer.parseInt(map.get("age")) < 21) {
                hsl.add(map);
            }
        }
        System.out.println(hsl);
    }

    public static ArrayList<HashMap<String, String>> fileJson() {
        ArrayList<HashMap<String, String>> arr = new ArrayList<>();
        HashMap<String, String> map = new HashMap<>();
        map.put("id", "1");
        map.put("name", "Udin");
        map.put("age", "12");
        arr.add(map);

        map = new HashMap<>();
        map.put("id", "2");
        map.put("name", "Reane");
        map.put("age", "51");
        arr.add(map);

        map = new HashMap<>();
        map.put("id", "3");
        map.put("name", "Budi");
        map.put("age", "34");
        arr.add(map);

        map = new HashMap<>();
        map.put("id", "4");
        map.put("name", "Agus");
        map.put("age", "16");
        arr.add(map);

        map = new HashMap<>();
        map.put("id", "5");
        map.put("name", "Sari");
        map.put("age", "19");
        arr.add(map);

        map = new HashMap<>();
        map.put("id", "6");
        map.put("name", "Ririn");
        map.put("age", "20");
        arr.add(map);

        map = new HashMap<>();
        map.put("id", "7");
        map.put("name", "Dessy");
        map.put("age", "23");
        arr.add(map);

        return arr;
    }
}