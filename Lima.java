import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Lima {
    public static void main(String[] args) throws IOException {
        InputStreamReader r = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(r);

        int awal, akhir;
        ArrayList<Integer> arr = new ArrayList<>();

        System.out.print("Masukkan tahun awal = ");
        awal = Integer.parseInt(br.readLine());
        System.out.print("Masukkan tahun akhir = ");
        akhir = Integer.parseInt(br.readLine());

        for (int i = awal; i <= akhir; i++) {
            if (i % 4 == 0) {
                if (i % 100 == 0) {
                    if (i % 400 == 0) {
                        arr.add(i);
                    }
                } else {
                    arr.add(i);
                }
            }
        }

        System.out.println("List dari tahun-tahun kabisat diantara dua input tahun tersebut : ");
        for (int a : arr) {
            System.out.println(a);
        }
    }
}