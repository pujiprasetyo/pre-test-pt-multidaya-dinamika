import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Empat {
    public static void main(String[] args) throws IOException {
        InputStreamReader r = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(r);

        String awal, balik, hsl = "";

        System.out.print("Masukkan kata / kalimat Awal = ");
        awal = br.readLine();
        System.out.print("Masukkan kata / kalimat Kebalikkannya = ");
        balik = br.readLine();

        for (int i = balik.length() - 1; i >= 0; i--) {
            hsl = hsl + balik.charAt(i);
        }

        if (awal.equalsIgnoreCase(hsl)) {
            System.out.println("Palindrom");
        } else {
            System.out.println("Bukan Palindrom");
        }
    }
}